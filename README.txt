Views 3.x does not render localized taxonomy terms by default.

This simple module makes views render the taxonomy terms as localized, if the translation method for their vocabulary has been set as 'Localize'.

If a localized version for the term does not exist, the term will be displayed in the default language.

Module's dependencies:

    - Views
    - i18n Taxonomy (submodule from i18n package)

Please note that this module only applies if the translation for the desired vocabularies are handled by i18n_taxonomy module and 'Localize' translation mode has been selected.